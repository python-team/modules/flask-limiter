Source: flask-limiter
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Nicolas Dandrimont <olasd@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 furo <!nodoc>,
 pybuild-plugin-pyproject,
 python-flask-doc <!nodoc>,
 python-flask-restful-doc <!nodoc>,
 python-limits-doc <!nodoc>,
 python-pymongo-doc <!nodoc>,
 python-rediscluster-doc <!nodoc>,
 python-werkzeug-doc <!nodoc>,
 python3-all,
 python3-doc <!nodoc>,
 python3-flask,
 python3-flask-restful <!nocheck>,
 python3-hiro <!nocheck>,
 python3-limits (>= 3.13.0),
 python3-ordered-set <!nocheck>,
 python3-pymemcache <!nocheck>,
 python3-pymongo <!nocheck>,
 python3-pytest <!nocheck>,
 python3-redis <!nocheck>,
 python3-rich <!nodoc>,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-inline-tabs <!nodoc>,
 python3-sphinx-issues (>= 4.1.0),
 python3-sphinx-paramlinks,
 python3-sphinxcontrib.programoutput,
 python3-sphinxext-opengraph,
 python3-typing-extensions,
 python3-werkzeug,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/python-team/packages/flask-limiter.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/flask-limiter
Homepage: https://github.com/alisaifee/flask-limiter

Package: python-flask-limiter-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Rate-limiting for Flask routes - Documentation
 Flask is a micro web framework for Python based on Werkzeug, Jinja 2 and good
 intentions.
 .
 Flask-Limiter provides rate limiting features to flask routes. It has support
 for a configurable backend for storage with current implementations for
 in-memory, redis and memcache.
 .
 This package provides the documentation for the flask_limiter module.

Package: python3-flask-limiter
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 ${python3:Recommends},
Suggests:
 python-flask-limiter-doc,
 ${python3:Suggests},
Description: Rate-limiting for Flask routes
 Flask is a micro web framework for Python based on Werkzeug, Jinja 2 and good
 intentions.
 .
 Flask-Limiter provides rate limiting features to flask routes. It has support
 for a configurable backend for storage with current implementations for
 in-memory, redis and memcache.
 .
 This package provides the Python 3 version of the flask_limiter module.
